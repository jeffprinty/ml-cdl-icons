var fs = require('fs');
var DOMParser = require('xmldom').DOMParser;
var XMLSerializer = require('xmldom').XMLSerializer;
var serializer = new XMLSerializer();


function readFiles(dirname, onFileContent, onError) {
  fs.readdir(dirname, function(err, filenames) {
    if (err) {
      onError(err);
      return;
    }
    filenames.forEach(function(filename) {
      fs.readFile(dirname + filename, 'utf-8', function(err, content) {
        if (err) {
          onError(err);
          return;
        }
        onFileContent(filename, content);
      });
    });
  });
}
readFiles('oldsvg/',function(name,content){
  //console.log(name,content);
  var doc = new DOMParser().parseFromString(content, "image/svg+xml");
  //doc.documentElement.setAttribute('title',name);
  //console.log("was "+ doc.documentElement.getElementsByTagName("title")[0].childNodes[0].nodeValue,name.slice(0,-4));
  doc.documentElement.getElementsByTagName("title")[0].childNodes[0].data = name.slice(0,-4);
  //console.log(doc.documentElement.getElementsByTagName("title")[0].childNodes[0].nodeValue );
  var newDoc = serializer.serializeToString(doc);
  //console.log(doc.documentElement.getElementsByTagName("title")[0].childNodes[0]);
  fs.writeFileSync("svg/"+name, newDoc, function(err) {
      if(err) {
          return console.log(err);
      }

      //console.log(name + " was saved!");
  }); 
  //console.log(name, doc.title );
})